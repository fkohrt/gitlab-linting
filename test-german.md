---
lang: de-DE
---

Eine Glückswunsch an die Reiswaffelfabrikanten**tochter**, inklusive Widerholung und Bindestrick. SchülerInnen kamen nicht.

Die systemische Gewalt hört nicht beim Zwang auf, sich den herrschenden Verhältnissen zu unterwerfen und ein Leben zu führen, das diesen entspricht. Die gesellschaftliche Ordnung nötigt Individuen nicht bloß zu einem systemgemäßen Verhalten, sie zwingt ihnen auch eine entsprechende Haltung auf. Um als vernünftiges Gesellschaftsmitglied anerkannt zu werden, gilt es, die herrschende Ordnung „zu integrieren“. Es geht darum, die gesellschaftlichen Prämissen als „natürlich“ anzuerkennen und sich nur innerhalb eines Verhaltensspektrums wohl zu fühlen, das mit diesen korreliert. Gesellschaftsmitglied sein heißt niemals bloß dem gesellschaftlichen System unterworfen zu sein, es heißt zugleich auch immer, Träger desselben zu sein.

[Actually, multiple languages in the same document are supported.]{lang=en-GB}
